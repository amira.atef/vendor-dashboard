module.exports = {
  "extends": "airbnb",
  "env": {
    "es6": true,
    "browser": true,
    "jest": true
  },
  rules:{
    "linebreak-style": 0
  }};
